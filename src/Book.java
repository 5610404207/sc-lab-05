
public class Book {
	    String title;
	    String authors;
	    boolean borrowed;

	    public Book(String bookTitle,String bookAuthors) {
	        title = bookTitle;
	        authors = bookAuthors;
	        borrowed = false;
	    }
	   
	    public void borrowed() {
	        borrowed = true;
	    }
	   
	    public void rented() {
	        borrowed = true;
	    }

	    public void returned() {
	        borrowed = false;
	    }
	   
	    public  boolean isBorrowed() {
	        return borrowed;
	    }
	   
	    public String getTitle() {
	        return title;
	    }
	    
	    public String getAuthors(){
			return authors;
	    }

	

}
