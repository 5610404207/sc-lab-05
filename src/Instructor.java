import java.util.Map.Entry;


public class Instructor {

	private String nickName, firstName,id;
	private Library lib;
	
	public Instructor(String nickName, String firstName, String string) {
		this.nickName = nickName;
		this.firstName = firstName;
		lib = new Library();
		
	}

	public String getBorrowBook() {
		String bookName = "";
		for (Entry<Instructor, String> aBook : Library.borrowBook3.entrySet()) {
			if(aBook.getKey().getName() != null){
				bookName = aBook.getValue();
			}
		}
		return bookName;
	}

	public String getName() {
		return nickName+" "+firstName;
	}

}

