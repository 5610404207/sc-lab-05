
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class Library {
	public ArrayList<Book> bookList;
	public ArrayList<ReferencesBook> refBookList;
	public static HashMap<Student,String> borrowBook1;
	public static HashMap<Staff,String> borrowBook2;
	public static HashMap<Instructor,String> borrowBook3;
	
	public Library() {
		bookList = new ArrayList<Book>();
		refBookList = new ArrayList<ReferencesBook>();
		borrowBook1 = new HashMap<Student,String>();
		borrowBook2 = new HashMap<Staff,String>();
		borrowBook3 = new HashMap<Instructor,String>();
	}
	//Student ----
    public void borrowBook(Student stu, String title) {
        int count = 0;
        int ans = 0;
      
		for (Book book : bookList) {
            if (book.getTitle().equals(title)) {
                if (count == 0) {
                	ans = 1;
                    }
                if (!book.isBorrowed()) {
                    book.borrowed();
                    ans = 2;
                    break;
                };
            }
        }
        if (ans == 0) {
            System.out.println("This book is not in catalog");
        } else if (ans == 1) {
            System.out.println("This book is already borrowed");
        } else if (ans == 2) {
            System.out.println(stu.getName() + " successfull borrowed " + title);
            borrowBook1.put(stu, title);
        }
    }

    public void returnBook(Student stu, String title) {
        boolean count = false;
        for (Book book : bookList) {
            if (book.getTitle().equals(title) && book.isBorrowed()) {
                book.returned();
                count = true;
                break;
            }
        }
        if (count) {
           System.out.println(stu.getName() + " successfull returned " + title);
           borrowBook2.remove(stu, title);
        }
    }
    
    //Staff -----
    public void borrowBook(Staff staff, String title) {
        int count = 0;
        int ans = 0;
		for (Book book : bookList) {
            if (book.getTitle().equals(title)) {
                if (count == 0) {
                	ans = 1;
                    }
                if (!book.isBorrowed()) {
                    book.borrowed();
                    ans = 2;
                    break;
                };
            }
        }
        if (ans == 0) {
            System.out.println("This book is not in catalog");
        } else if (ans == 1) {
            System.out.println("This book is already borrowed");
        } else if (ans == 2) {
            System.out.println(staff.getName() + " successfull borrowed " + title);
            borrowBook2.put(staff, title);
        }
    }

    public void returnBook(Staff staff, String title) {
        boolean count = false;
        for (Book book : bookList) {
            if (book.getTitle().equals(title) && book.isBorrowed()) {
                book.returned();
                count = true;
                break;
            }
        }
        if (count) {
           System.out.println(staff.getName() + " successfull returned " + title);
           borrowBook2.remove(staff, title);
        }
    }
    //Instructor ----
    public void borrowBook(Instructor ins, String title) {
        int count = 0;
        int ans = 0;
		for (Book book : bookList) {
            if (book.getTitle().equals(title)) {
                if (count == 0) {
                	ans = 1;
                    }
                if (!book.isBorrowed()) {
                    book.borrowed();
                    ans = 2;
                    break;
                };
            }
        }
        if (ans == 0) {
            System.out.println("This book is not in catalog");
        } else if (ans == 1) {
            System.out.println("This book is already borrowed");
        } else if (ans == 2) {
            System.out.println(ins.getName() + " successfull borrowed " + title);
            borrowBook3.put(ins, title);
        }
    }

    public void returnBook(Instructor ins, String title) {
        boolean count = false;
        for (Book book : bookList) {
            if (book.getTitle().equals(title) && book.isBorrowed()) {
                book.returned();
                count = true;
                break;
            }
        }
        if (count) {
           System.out.println(ins.getName() + " successfull returned " + title);
           borrowBook3.remove(ins, title);
        }
    }
    public void addBook(Book aBook) {
    	bookList.add(aBook);
    }

    public void addRefBook(ReferencesBook refBook) {
    	refBookList.add(refBook);
    }

	public String bookCount() {
		return "Book count : "+bookList.size();
	}

	public String getBorrower1() {
		String allName = "";
		for (Entry<Student, String> aBook : borrowBook1.entrySet()) {
			allName = aBook.getKey().getName() +" ";
		}
		
		return allName;
	}
	
	public String getBorrower2() {
		String allName = "";
		for (Entry<Staff, String> aBook : borrowBook2.entrySet()) {
			allName = aBook.getKey().getName() +" ";
		}
		
		return allName;
	}
	
	public String getBorrower3() {
		String allName = "";
		for (Entry<Instructor, String> aBook : borrowBook3.entrySet()) {
			allName = aBook.getKey().getName() +" ";
		}
		
		return allName;
	}

}
