import java.util.Map.Entry;


public class Staff {
	private String nickName, firstName;
	private Library lib;
	
	public Staff(String nickName, String firstName) {
		this.nickName = nickName;
		this.firstName = firstName;
		lib = new Library();
		
	}

	public String getBorrowBook() {
		String bookName = "";
		for (Entry<Staff, String> aBook : Library.borrowBook2.entrySet()) {
			if(aBook.getKey().getName() != null){
				bookName = aBook.getValue();
			}
		}
		return bookName;
	}

	public String getName() {
		return nickName+" "+firstName;
	}

}