import java.util.Map.Entry;


public class Student {
	private String nickName, firstName, id;
	private Library lib;
	
	public Student(String nickName, String firstName, String id) {
		this.nickName = nickName;
		this.firstName = firstName;
		this.id = id;
		lib = new Library();
		
	}

	public String getBorrowBook() {
		String bookName = "";
		for (Entry<Student, String> aBook : Library.borrowBook1.entrySet()) {
			if(aBook.getKey().getName() != null){
				bookName = aBook.getValue();
			}
		}
		return bookName;
	}

	public String getName() {
		return nickName+" "+firstName;
	}

}
