
public class TestBook {

	public static void main(String[] args) {
        Library lib = new Library();
        Student stu = new Student("Sun","Kittipat","5610404207");
        Instructor ins = new Instructor("Chu","Chureerat","D14");
        Staff staff = new Staff("Gaew","FirstName");
        
        Book book = new Book("Big Java","Watson");
        ReferencesBook refBook = new ReferencesBook("ABC","John");
        
        lib.addBook(book);
        lib.addRefBook(refBook);
        
        //test1 Student
        System.out.println(lib.bookCount());
        System.out.println("Book name : "+stu.getBorrowBook());
        System.out.println("Borrower : " + lib.getBorrower1());
        System.out.println("Name :" + stu.getName());

        lib.borrowBook(stu, "Big Java");
        
        System.out.println(lib.bookCount());
        System.out.println("Borrow Book : " + stu.getBorrowBook());
        System.out.println("Borrower : " + lib.getBorrower1());
        
        lib.returnBook(stu, "Big Java");
        
        //test2 Staff
        System.out.println("");
        System.out.println(lib.bookCount());
        System.out.println("Book name : "+ staff.getBorrowBook());
        System.out.println("Borrower : " + lib.getBorrower2());
        System.out.println("Name : " + staff.getName());
     
        lib.borrowBook(staff, "Big Java");
        
        System.out.println(lib.bookCount());
        System.out.println("Borrow Book : " + staff.getBorrowBook());
        System.out.println("Borrower : " + lib.getBorrower2());
        
        lib.returnBook(staff, "Big Java");
        
        //test3 Instructor
        System.out.println("");
        System.out.println(lib.bookCount());
        System.out.println("Book name : "+ ins.getBorrowBook());
        System.out.println("Borrower : " + lib.getBorrower3());
        System.out.println("Name : " + ins.getName());
     
        lib.borrowBook(ins, "Big Java");
        
        System.out.println(lib.bookCount());
        System.out.println("Book name : "+ ins.getBorrowBook());
        System.out.println("Borrower : " + lib.getBorrower3());
        
        lib.returnBook(ins, "Big Java");
        
        lib.borrowBook(ins, "ABC");
        
        
	}

}
